<?php

//session engine
session_start();
if( ! isset($_SESSION['name']) ){
  header('Location: /../login.php');
}

try{
    $sTweetId = uniqid();
  
    if( ! isset($_POST['tweetText']) ){
      http_response_code(400);
      header('Content-Type: application/json');
      echo '{"error":"missing text"}';
      exit();
    }
    if( strlen($_POST['tweetText']) < 1 ){
      http_response_code(400);
      header('Content-Type: application/json');
      echo '{"error":"text must be at least 1 characters"}';
      exit();
    }
    if( strlen($_POST['tweetText']) > 280 ){
      http_response_code(400);
      header('Content-Type: application/json');
      echo '{"error":"tweet cannot be longet than 280 characters"}';
      exit();
    }
  
// create tweet
  $jTweet             = new stdClass(); // {}
  $jTweet->user       = $_SESSION['id'];
  $jTweet->name       = $_SESSION['name'];
  $jTweet->profileIMG = $_SESSION['profileIMG'];
  $jTweet->id         = $sTweetId;
  $jTweet->text       = $_POST['tweetText'];
  $jTweet->img        = '';
  $jTweet->body       = '';
  $jTweet->comments   = 0;
  $jTweet->shares     = 0;
  $jTweet->loves      = 0;
  $jTweet->active     = 1;

// conect to db

  require_once(__DIR__.'/../private/db.php');
  $q = $db->prepare(
      'INSERT INTO `tweets` VALUES (:tweetId, :userId, :tweetText, NULL, NULL, "0", "0", "0", current_timestamp(), "1")'
  );
  $q->bindValue('tweetId',$sTweetId);
  $q->bindValue('userId',$_SESSION['id']);
  $q->bindValue('tweetText',$_POST['tweetText']);
  $q->execute();
  // $aTweets = $q->fetchAll();
  // echo json_encode($aTweets);

  echo json_encode($jTweet);
}

catch(Exception $ex){
  // http_response_code(500);
  // header('Content-Type: application/json');
  echo '{"message":"error '.__LINE__.'"}';
}
