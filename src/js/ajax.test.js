import { tweet } from './ajax'

beforeEach(() => {
  fetch.resetMocks()
})

function FormDataMock() {
  return {
    message: 'This is a test message'
  }
}

global.FormData = FormDataMock

it('Create tweet Proper response and only once', async () => {
  // Mock document element to use insertadjacentElement
  document.body.innerHTML = '<div id="ProfileDb"></div>'

  // Generate FormData
  const formData = new FormData()
  const stringFormData = JSON.stringify({
    body: formData,
    method: 'POST'
  })
  fetch.mockResponseOnce(stringFormData)
  fetch.mockReturnValueOnce(formData)

  // Calling the fetch
  const makeTweet = await tweet()

  expect(makeTweet.body.message).toEqual('This is a test message')

  // // Checking the PHP API path
  // expect(fetch.mock.calls[0][0]).toEqual('../api/api-create-tweet.php')

  // // Only called once
  expect(fetch).toHaveBeenCalledTimes(1)
})

it('Create tweet AJAX calls using proper URI', async () => {
  // Mock document element to use insertadjacentElement
  document.body.innerHTML = '<div id="ProfileDb"></div>'

  // Generate FormData
  const formData = new FormData()
  const stringFormData = JSON.stringify({
    body: formData,
    method: 'POST'
  })
  fetch.mockResponseOnce(stringFormData)
  fetch.mockReturnValueOnce(formData)

  // Calling the fetch
  const makeTweet = await tweet()

  // Full fetch call correct
  expect(fetch).toHaveBeenCalledWith('../api/api-create-tweet.php', JSON.parse(stringFormData))
})
