<?php
use PHPUnit\Framework\TestCase;

class DBTest extends TestCase
{
    static private $pdo = null;

    private $conn = null;

    public function setUp(): void
    {
        $this->pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->query("CREATE TABLE hello (what VARCHAR(50) NOT NULL)");
    }

    public function tearDown(): void
    {
        if (!$this->pdo)
            return;
        $this->pdo->query("DROP TABLE hello");
    }


    public function testDate(): void
    {
        $this->assertEquals(date("Y-m-d"), $this->query("SELECT CURDATE();"));
    }
}


// SELECT CAST( GETDATE() AS Date ) ; 2019-08-17

// date("Y-m-d");

